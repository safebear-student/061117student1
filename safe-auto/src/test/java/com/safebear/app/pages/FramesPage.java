package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 06/11/2017.
 */
public class FramesPage {

    WebDriver driver;

    public FramesPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Frame Page");
    }
}
